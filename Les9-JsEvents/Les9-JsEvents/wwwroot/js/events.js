﻿let
    _ = (id) => document.getElementById(id),
    scrollDelay = 2000,
    marqueeSpeed = 1,
    timer,
    scrollArea,
    marquee,
    scrollPosition = 0;

let scrolling = () => {
    if (scrollPosition + scrollArea.offsetHeight <= 0) {
        scrollPosition = marquee.offsetHeight;
    } else {
        scrollPosition = scrollPosition - marqueeSpeed;
    }
    scrollArea.style.top = scrollPosition + "px";
};

let startScrolling = () => {
    timer = setInterval(scrolling, 30);
};

let initMarquee = () => {
    scrollArea = _('scroll-area');
    scrollArea.style.top = 0;

    marquee = _('marquee');
    setTimeout(startScrolling, scrollDelay);
};

let pauseMarquee = () => {
    if (marqueeSpeed > 0) {
        marqueeSpeed = 0;
    } else {
        marqueeSpeed = 1;
    }
};

let speedUpMarquee = () => {
    if (marqueeSpeed <= 3) {
        marqueeSpeed++;
    }
};

let slowDownpMarquee = () => {
    if (marqueeSpeed > 1) {
        marqueeSpeed--;
    }
};


// TEOGEVOEGD NA LES 9 om te spelen met event/target
let adjustScrollSpeed = (event) => {

    let target = event.target;

    if (target.tagName === 'BUTTON') {
        if (target.id === 'btnSpeedUp') {
            speedUpMarquee();

        } else if (target.id === 'btnSlowDown') {
            slowDownpMarquee();
        }
    }
};


// om events te kunnen chainen
EventTarget.prototype.addEventListener = (() => {
    const addEventListener = EventTarget.prototype.addEventListener;

    return function () {
        addEventListener.apply(this, arguments);
        return this;
    };
})();

EventTarget.prototype.removeEventListener = (() => {
    const removeEventListener = EventTarget.prototype.removeEventListener;

    return function () {
        removeEventListener.apply(this, arguments);
        return this;
    };
})();


window.addEventListener('DOMContentLoaded', () => {
    initMarquee();

    marquee
        .addEventListener('mouseover', pauseMarquee)
        .addEventListener('mouseout', pauseMarquee);

    // na lezen les 9 heb ik dit in commentaar gezet
    // _('btnSpeedUp').addEventListener('click', speedUpMarquee);
    // _('btnSlowDown').addEventListener('click', slowDownpMarquee);

    // TOEGEVOEGD om te spelen met event/target
    document.addEventListener('click', adjustScrollSpeed);

});

window.addEventListener('unload', () => {
    marquee
        .removeEventListener('mouseover', pauseMarquee)
        .removeEventListener('mouseout', pauseMarquee);

    // _('btnSpeedUp').removeEventListener('click', speedUpMarquee);
    // _('btnSlowDown').removeEventListener('click', slowDownpMarquee);

    document.removeEventListener('click', adjustScrollSpeed);
});

