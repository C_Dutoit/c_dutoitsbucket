﻿// IK HAD EERST DIT GEMAAKT EN HEB HIERNA OOK DE CLASS-CONSTRUCTOR GEMAAKT
// HEB IN CLASS_CONSTRUCTOR EEN AANTAL ZAKEN GEREFACTORD EN OOK VEEL
// GEBRUIK GEMAAKT VAN DE TERNAIRE OPERATOR. IK BEN DAAR ZOT VAN, MAAR WEET DAT
// BV TOM DAAR HELEMAAL NIET VAN HOUDT. DONT JUDGE ME. ;-)

function TableSort(id) {
    this.tableElement = document.getElementById(id);
    // heb voor de caret van font=awsome gekozen (zie HTML) => moet ook alle i-elementen hebben
    this.headingArrows = this.tableElement.querySelectorAll('th > i');
    // sortingOrder gebruik ik in de sorteer algoritmen
    this.sortingOrder = 'asc';

    if (this.tableElement && this.tableElement.nodeName == "TABLE") {
        this.prepare();
    }
}

TableSort.prototype.prepare = function () {
    let headings = this.tableElement.tHead.rows[0].cells;

    for (let i = 0; i < headings.length; i++) {
        headings[i].className = 'asc';
    }

    // in FIREFOX krijg ik de foutmelding dat "event" niet gekend is
    // in IE werkt de code helemaal niet
    // in CHROME en EDGE wel
    this.tableElement.addEventListener("click", function (that) {
        // console.log(event);
        return function () {
            that.eventHandler(event);
        }
    }(this), false);
}

// Om de richting van de caret te veranderen.
TableSort.prototype.changeArrow = (elem, up) => {
    let angleUp = up ? 'up' : 'down',
        angleDown = up ? 'down' : 'up';

    elem.classList.remove('fa-angle-' + angleUp);
    elem.classList.add('fa-angle-' + angleDown);
}

// als ik in een andere kolom klik, zet ik alle pijltjes eerst terug naar boven
// en maak ik alle classen terug 'asc'. ( ZIE eventhandler hieronder )
// In 'class-constructor.js' heb ik dit compacter gemaakt en mee verwerkt in changeArrow 
TableSort.prototype.resetArrows = function () {
    // werkte alleen in CHROME
    //this.headingArrows.forEach((element) => {
    //    this.changeArrow(element, false);
    //    element.parentNode.className = 'asc';
    //});

    for (let i = 0; i < this.headingArrows.length; i++) {
        this.changeArrow(this.headingArrows[i], false);
        this.headingArrows[i].parentNode.className = 'asc';
    }

}

TableSort.prototype.eventHandler = function (event) {
    // taget  = mijn i-element
    // parent = headingcell
    let target = event.target,
        parent = target.parentNode;

    if (target.tagName === 'I') {
        if (parent.tagName === 'TH') {
            
            let isAsc = parent.className === 'asc';

            this.resetArrows();

            parent.className = isAsc ? 'desc' : 'asc';
            this.sortingOrder = isAsc ? 'desc' : 'asc';

            this.changeArrow(target, isAsc ? true : false);
            this.sortColumn(parent);
            
            
            
//            if (parent.className === 'asc') {
//
//                this.resetArrows();
//
//                parent.className = 'desc';
//                this.sortingOrder = 'desc';
//
//                this.changeArrow(target, true);
//                this.sortColumn(parent);
//
//            } else {
//                this.resetArrows();
//
//                parent.className = 'asc';
//                this.sortingOrder = 'asc';
//
//                this.changeArrow(target, false);
//                this.sortColumn(parent);
//            };
        }
    }
}



TableSort.prototype.sortColumn = function (headerCell) {
    let rows = this.tableElement.rows,
        alpha = [],
        numeric = [],
        alphaIndex = 0,
        numericIndex = 0,
        cellIndex = headerCell.cellIndex,
        orderdedColumns = []


    for (let i = 1; rows[i]; i++) {

        let cell = rows[i].cells[cellIndex],
            content = cell.textContent ? cell.textContent : cell.innerText,
            numericValue = content.replace(/(\$|\,|\s)/g, "");

        if (parseFloat(numericValue) == numericValue) {
            numeric[numericIndex++] = {
                value: +(numericValue),
                row: rows[i]
            }
        }
        else {
            alpha[alphaIndex++] = {
                value: content,
                row: rows[i]
            }
        }
    }

    numeric.sort((a, b) => {
        return this.sortingOrder === "asc" ?
            a.value - b.value : b.value - a.value;
    });

    alpha.sort((a, b) => {
        let aName = a.value.toLowerCase(),
            bName = b.value.toLowerCase(),
            // om de sorteer richting te bepalen
            order = this.sortingOrder === "asc";

        if (aName < bName) {
            return order ? -1 : 1;
        } else if (aName > bName) {
            return order ? 1 : -1;
        } else {
            return 0;
        }
    });

    orderdedColumns = numeric.concat(alpha);
    let tBody = this.tableElement.tBodies[0];

    for (let i = 0; orderdedColumns[i]; i++) {
        tBody.appendChild(orderdedColumns[i].row);
    }
}


window.addEventListener('DOMContentLoaded', () => {
    let jommeke = new TableSort('jommeke');
    let fruit = new TableSort('fruit');
});