﻿// VERSCHILLEND VAN CONSTRUCTOR-FUNCTION.js: XXXXXXXXXXXXX
class TableSort {
    constructor(id) {
        this.tableElement = document.getElementById(id);
        // heb voor de caret van font=awsome gekozen (zie HTML) => moet ook alle i-elementen hebben
        this.headingArrows = this.tableElement.querySelectorAll('th > i');
        // sortingOrder gebruik ik in de sorteer algoritmen
        this.sortingOrder = 'asc';

        // XXXXXXXXXXXXX
        // om vorige pijltje en class terug juist te zetten
        this.prevTarget = null;

        if (this.tableElement && this.tableElement.nodeName == "TABLE") {
            this.prepare();
        }
    }

    prepare() {
        let headings = this.tableElement.tHead.rows[0].cells;

        for (let i = 0; i < headings.length; i++) {
            headings[i].className = 'asc';
        }

        this.tableElement.addEventListener("click", function (that) {
            // console.log(event);
            return function () {
                that.eventHandler(event);
                return false;
            }
        }(this), false);
    }

    // XXXXXXXXXXXXX
    // Om de richting van de caret te veranderen.
    // resetArrow wordt nu hier afgehandeld
    changeArrow(elem, up, previous) {
        if (elem) {
            let angleUp = up ? 'up' : 'down',
                angleDown = up ? 'down' : 'up';

            elem.classList.remove('fa-angle-' + angleUp);
            elem.classList.add('fa-angle-' + angleDown);

            if (previous) {
                elem.parentNode.className = 'asc';
            }
        }
    }

    sortColumn(headerCell) {
        let rows = this.tableElement.rows,
            alpha = [],
            numeric = [],
            alphaIndex = 0,
            numericIndex = 0,
            cellIndex = headerCell.cellIndex,
            orderdedColumns = []


        for (let i = 1; rows[i]; i++) {

            let cell = rows[i].cells[cellIndex],
                content = cell.textContent ? cell.textContent : cell.innerText,
                numericValue = content.replace(/(\$|\,|\s)/g, "");

            if (parseFloat(numericValue) == numericValue) {
                numeric[numericIndex++] = {
                    value: +(numericValue),
                    row: rows[i]
                }
            }
            else {
                alpha[alphaIndex++] = {
                    value: content,
                    row: rows[i]
                }
            }
        }

        numeric.sort((a, b) => {
            return this.sortingOrder === "asc" ?
                a.value - b.value : b.value - a.value;
        });

        alpha.sort((a, b) => {
            let aName = a.value.toLowerCase(),
                bName = b.value.toLowerCase(),
                // om de sorteer richting te bepalen
                order = this.sortingOrder === "asc";

            if (aName < bName) {
                return order ? -1 : 1;
            } else if (aName > bName) {
                return order ? 1 : -1;
            } else {
                return 0;
            }
        });

        orderdedColumns = numeric.concat(alpha);
        let tBody = this.tableElement.tBodies[0];

        for (let i = 0; orderdedColumns[i]; i++) {
            tBody.appendChild(orderdedColumns[i].row);
        }
    }

    //XXXXXXXXXXXXX
    eventHandler() {
        // taget  = mijn i-element
        // parent = headingcell van i-elem
        let target = event.target,
            parent = target.parentNode;

        if (target.tagName === 'I') {
            if (parent.tagName === 'TH') {
                let up = parent.className === 'asc',
                    isDiff = this.prevTarget !== target;

                parent.className = up ? 'desc' : 'asc';
                this.sortingOrder = up ? 'desc' : 'asc';

                // als parent != previous-parent zet ik pijltje terug naar boven
                // en maak ik class = 'asc' in changeArrow
                isDiff ?
                    this.changeArrow(this.prevTarget, up ? false : true, this.prevTarget != null) :
                    null;

                this.changeArrow(target, up ? true : false);
                this.sortColumn(parent);
            }
            // om te weten op welk vorig pijltje ik gecliked had
            this.prevTarget = target;
        }
    }
}


window.addEventListener('DOMContentLoaded', () => {
    let jommeke = new TableSort('jommeke');
    let fruit = new TableSort('fruit');
});