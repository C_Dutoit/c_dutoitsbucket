// // https://developers.google.com/identity/sign-in/web/reference 

let googleApi = (() => {
   let _ = (id) => document.getElementById(id),
      getElem = (klas) => document.querySelector(klas),
      apiKey = 'AIzaSyBO3-yY0RjKx25vKio5gTIg9DqLPqS2r34',
      clientId = '639408070243-b2soc5tehhqhh2ncfalhrjj66v3jl8oo.apps.googleusercontent.com',
      scopes = 'profile',
      signinButton = _('signin-button'),
      logoutButton = _('a-logout'),
      loginButton = _('a-login'),

      hideOrSeek = (elem, modus) => {
         elem.style.display = modus;
      },

      showOverlay = (show) => {
         let film = getElem('.film');
         show ? film.classList.add('overlay') : film.classList.remove('overlay');
      },

      changeStateOfApplication = (normal) => {
         showLoginDialog(false);
         hideOrSeek(_('content'), normal ? 'none' : 'block');
         hideOrSeek(loginButton, normal ? 'block' : 'none');
         hideOrSeek(logoutButton, normal ? 'none' : 'block');
      },

      showLoginDialog = (show) => {
         let dialog = getElem('.inlog-dialog');

         showOverlay(show ? true : false);

         dialog.style.opacity = show ? 1 : 0;
         dialog.style.display = show ? 'block' : 'none';
      },

      signIn = () => {
         gapi.auth2.getAuthInstance().signIn();
      },

      signOut = (event) => {
         gapi.auth2.getAuthInstance().signOut();
         changeStateOfApplication(true)

         console.log('user logged out');
      },

      showInfo = (code, detail) => {
         // Paa = code voor de image-url
         // API returnt '.' voor data dat er niet is
         if (code !== 'Paa') {
            _(code).innerHTML = detail === '.' ? 'Not Found' : detail;
         } else {
            // Not Found voor de foto wordt afgehandeld in de HTML -> alt="No picture found"
            _(code).src = detail
         }
      },

      initLoginButton = (authInstance, btn) => {
         authInstance.attachClickHandler(
            btn, {},
            (currentUser) => {
               // console.log(gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile());

               changeStateOfApplication(false);

               let profile = currentUser.getBasicProfile();

               // ik gebruik de codes die door de API worden teruggestuurd: Paa, Eea, ig, wea, ofa, U3
               // filter de functies (getName, getEmail, ...) eruit. Had die uiteraard ook kn gebruiken.
               // weet dat die er zijn maar wou iets anders doen.
               for (code in profile) {
                  if (typeof profile[code] !== 'function') {
                     showInfo(code, profile[code])
                  }
               }
            },
            (error) => {
               _('container').innerHTML = "No such person found";
               // eventueel verdere/betere error afhandeling ...
            });
      },

      initAuth = () => {
         gapi.auth2.init({
            client_id: clientId,
            scope: scopes
         })
            .then((authInstance) => {
               initLoginButton(authInstance, signinButton)
            })
      },

      startApp = () => {
         gapi.load('client:auth2', initAuth);
      },

      init = () => {
         logoutButton.addEventListener('click', signOut);
         loginButton.addEventListener('click', showLoginDialog);
         startApp();
      };

   return {
      init: init
   }
})();

window.addEventListener('DOMContentLoaded', googleApi.init);
