﻿const basePath = 'https://gateway.marvel.com:443/v1/public/characters',
    apiKey = '&apikey=66505a43f99d2d29f6bc273fd7ffb4e0';

let
    _ = (id) => document.getElementById(id),
    getAll = (elem) => document.querySelectorAll(elem),
    create = (elem) => document.createElement(elem),
    setTitel = (titel) => getAll('.comic-card > H2')[0].innerHTML = titel;


let showOverlay = (show) => {
    let overlay = _('overlay'),
        container = _('container'),
        defaultHeight = 200,
        // om te weten waar ik mijn modal moet tonen als er gescrolld werd
        offset = window.pageYOffset;

    show ? overlay.classList.add('overlay') : overlay.classList.remove('overlay')
    overlay.style.height = show ? (container.clientHeight) + 'px' : '0px';
    // modal met titels tonen op defaultHeight (200) + offset
    getAll('.comic-card')[0].style.top = show ? (defaultHeight + offset) + 'px' : '-100%';
}

let getTitels = (e) => {
    let opa = e.target.parentNode.parentNode,
        id = opa.childNodes[1].value,
        naam = opa.childNodes[0].childNodes[1].innerHTML;

    const path = basePath + `?id=${id}` + apiKey;

    DataContext(path).ajaxGetService()
        .then((response) => {
            let ul = _('titels'),
                details = response.data.results[0].comics.items;

            ul.innerHTML = '';

            if (details.length) {
                details.map((detail) => {
                    let li = create('li');
                    li.innerHTML = detail.name;
                    ul.appendChild(li);
                })
                setTitel('Comic strips van ' + naam + ':');
            } else {
                setTitel('Geen titels gevonden');
            }
            showOverlay(true);

        }).catch(() => {
            setTitel('Error. Probeer opnieuw.');
            showOverlay(true);
        });
}


let showMarvelCharacters = {

    error: (response) => {
        let pre = create('PRE'),
            t = document.createTextNode('error: ' + response);

        pre.appendChild(t);
        document.body.appendChild(pre);
    },

    succes: (response) => {
        let characters = response.data.results,
            div = create('div'),
            container = _('container'),
            fillUp = document.getElementsByClassName('fill-up')[0];

        // text 'fotootjes komen zo' in de HTMl laten verdwijnen.
        _('in-afwachting').style.display = 'none';

        // Er zijn enkele images die text 'image_not_available' in hun path hebben.
        // die ga ik eruit filteren. Er blijven er echter wel 2 instaan omdat dat .gif zijn.
        // kan je in de json zien die terug komt van de API.
        let filteredChars = (char) => !char.thumbnail.path.includes("image_not_available");

        characters
            .filter((character) => {
                return filteredChars(character);
            }).map((character) => {
                let div = create('div'),
                    image = create('img'),
                    figure = create('figure'),
                    figCaption = create('figcaption'),
                    input = create('input'),
                    titel = document.createTextNode(character.name);

                image.src = character.thumbnail.path + '.' + character.thumbnail.extension;
                image.alt = 'Image of a Marvel Super Hero.';
                // click event toevoegen aan imgages => geeft ID van personage
                image.addEventListener('click', (e) => {
                    getTitels(e);
                });

                figCaption.appendChild(titel);

                figure.appendChild(image);
                figure.appendChild(figCaption);

                // Hidden input met ID va het personage
                // => als ik op de image click heb ik het ID en kan ik de comics opvragen
                // van 1 enkel personage.
                input.type = 'hidden';
                input.value = character.id;

                div.appendChild(figure);
                div.appendChild(input);

                // ik insert alles voor de eerste div met class 'fill-up' => alles blijft alfabetisch
                container.insertBefore(div, fillUp);
            })
    }
}


window.addEventListener('DOMContentLoaded', () => {
    const path = basePath + "?limit=100" + apiKey;

    DataContext(path).ajaxGetService()
        .then((data) => {
            showMarvelCharacters.succes(data);
        }).catch(showMarvelCharacters.error);

    getAll('.btn-close')[0]
        .addEventListener('click', () => {
            showOverlay(false);
        });
});