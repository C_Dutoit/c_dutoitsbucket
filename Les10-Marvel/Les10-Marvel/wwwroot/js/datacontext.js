﻿/*
    Geïnspireerd door Josef Inghelbrecht, leraar CVO Antwerpen
*/

/*
    je kan in een van mijn vorige oefeningen al zien dat ik deze dataContext gebruik.
    Had die vorig jaar in module 4 gemaakt. Allen heb ik toen de POST nog met Jquery gadaan omdat het me niet lukte een
    POST in puur JS te maken met PROMISE. Ik heb me dus laten inspireren door de code die ik van u gekregen heb. Tnx! ;-)
*/
let DataContext = (path) => {

    //let baseUrl = "http://localhost:56898/";
    let baseUrl = "";

    let ajaxService = (methode, args) => {

        let xhr = new XMLHttpRequest(),
            url = baseUrl + path,
            method = methode;

        return new Promise((resolve, reject) => {

            if (args && (method === 'POST' || method === 'PUT')) {
                url += '?';
                let argcount = 0;

                for (let key in args) {
                    if (args.hasOwnProperty(key)) {

                        if (argcount++) {
                            url += '&';
                        }
                        url += encodeURIComponent(key) + '=' + encodeURIComponent(args[key]);
                    }
                }
            }

            xhr.open(method, url, true);
            xhr.responseType = 'text';

            xhr.onload = () => {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                } else {
                    reject(xhr.statusText + xhr.responseText);
                }
            };

            xhr.onerror = () => {
                reject(xhr.statusText);
            };

            xhr.send();
        });
    };

    return {
        ajaxGetService: () => ajaxService('GET'),
        ajaxPostService: (args) => ajaxService('POST', args),
        ajaxPutService: (args) => ajaxService('PUT', args),
        ajaxDeleteService: (args) => ajaxService('DELETE', args)
    };
};
