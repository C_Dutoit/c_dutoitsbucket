﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
           => View();
    }
}
